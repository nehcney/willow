# Standard imports
from datetime import datetime
import traceback
import logging

# Local imports
from utils import (
    get_gmaps_link,
    get_applemaps_link,
    # get_pokemon_size,
    get_pokemon_gender
)


logger = logging.getLogger('willow')


class Map:
    def __init__(self):
        raise NotImplementedError("This is a static class not meant to be initiated")

    @staticmethod
    def make_object(data):
        try:
            kind = data.get('type')
            if kind == 'pokemon':
                return Map.pokemon(data.get('message'))
            elif kind == 'raid':
                return Map.raid(data.get('message'))
            elif kind in ['captcha', 'scheduler', 'gym', 'pokestop']:  # Unsupported Webhooks
                logger.error("{} webhook received. This webhook is not supported at this time.".format({kind}))
            else:
                logger.error("Invalid type specified ({}). Are you using the correct map type?".format(kind))
        except Exception as e:
            logger.error("Encountered error while processing webhook ({}: {})".format(type(e).__name__, e))
            logger.error("Stack trace: \n {}".format(traceback.format_exc()))
        return None

    @staticmethod
    def pokemon(data):
        # logger.debug("Converting to pokemon: \n {}".format(data))
        quick_id = check_for_none(int, data.get('move_1'), '?')
        charge_id = check_for_none(int, data.get('move_2'), '?')
        lat, lng = data['latitude'], data['longitude']
        pkmn = {
            'type': "pokemon",
            'id': data['encounter_id'],
            'pkmn_id': int(data['pokemon_id']),
            'disappear_time': datetime.utcfromtimestamp(data['disappear_time']),
            'lat': float(data['latitude']),
            'lng': float(data['longitude']),
            'cp': check_for_none(int, data.get('cp'), '?'),
            'level': check_for_none(int, data.get('pokemon_level'), '?'),
            'iv': '?',
            'atk': check_for_none(int, data.get('individual_attack'), '?'),
            'def': check_for_none(int, data.get('individual_defense'), '?'),
            'sta': check_for_none(int, data.get('individual_stamina'), '?'),
            'quick_id': quick_id,
            #'quick_damage': get_move_damage(quick_id),
            #'quick_dps': get_move_dps(quick_id),
            #'quick_duration': get_move_duration(quick_id),
            #'quick_energy': get_move_energy(quick_id),
            'charge_id': charge_id,
            #'charge_damage': get_move_damage(charge_id),
            #'charge_dps': get_move_dps(charge_id),
            #'charge_duration': get_move_duration(charge_id),
            #'charge_energy': get_move_energy(charge_id),
            #'height': check_for_none(float, data.get('height'), 'unkn'),
            #'weight': check_for_none(float, data.get('weight'), 'unkn'),
            'gender': get_pokemon_gender(check_for_none(int, data.get('gender'), '?')),
            #'size': 'unknown',
            #'tiny_rat': '',
            #'big_karp': '',
            'gmaps': get_gmaps_link(lat, lng),
            'applemaps': get_applemaps_link(lat, lng)
        }
        if pkmn['atk'] != '?' and pkmn['def'] != '?' and pkmn['sta'] != '?':
            pkmn['iv'] = float(((pkmn['atk'] + pkmn['def'] + pkmn['sta']) * 100) / float(45))
        else:
            pkmn['atk'], pkmn['def'], pkmn['sta'] = '?', '?', '?'

        # if pkmn['height'] != 'unkn' or pkmn['weight'] != 'unkn':
        #     pkmn['size'] = get_pokemon_size(pkmn['pkmn_id'], pkmn['height'], pkmn['weight'])
        #     pkmn['height'] = "{:.2f}".format(pkmn['height'])
        #     pkmn['weight'] = "{:.2f}".format(pkmn['weight'])

        # if pkmn['pkmn_id'] == 19 and pkmn['size'] == 'tiny':
        #     pkmn['tiny_rat'] = 'tiny'

        # if pkmn['pkmn_id'] == 129 and pkmn['size'] == 'big':
        #     pkmn['big_karp'] = 'big'

        return pkmn

    @staticmethod
    def raid(data):
        quick_id = check_for_none(int, data.get('move_1'), '?')
        charge_id = check_for_none(int, data.get('move_2'), '?')

        raid_end = None
        raid_begin = None

        if 'raid_begin' in data:
            raid_begin = datetime.utcfromtimestamp(data['raid_begin'])
        elif 'battle' in data:
            raid_begin = datetime.utcfromtimestamp(data['battle'])
        elif 'start' in data:
            raid_begin = datetime.utcfromtimestamp(data['start'])

        if 'raid_end' in data:  # monocle
            raid_end = datetime.utcfromtimestamp(data['raid_end'])
        elif 'end' in data:  # rocketmap
            raid_end = datetime.utcfromtimestamp(data['end'])

        if 'raid_seed' in data:  # monocle sends a unique raid seed
            raid_seed = data.get('raid_seed')
        else:
            raid_seed = data.get('gym_id')  # RM sends the gym id

        raid = {
            'type': 'raid',
            'id': raid_seed,
            'pkmn_id': check_for_none(int, data.get('pokemon_id'), 0),
            'cp': check_for_none(int, data.get('cp'), '?'),
            'quick_id': quick_id,
            #'quick_damage': get_move_damage(quick_id),
            #'quick_dps': get_move_dps(quick_id),
            #'quick_duration': get_move_duration(quick_id),
            #'quick_energy': get_move_energy(quick_id),
            'charge_id': charge_id,
            #'charge_damage': get_move_damage(charge_id),
            #'charge_dps': get_move_dps(charge_id),
            #'charge_duration': get_move_duration(charge_id),
            #'charge_energy': get_move_energy(charge_id),
            'raid_level': check_for_none(int, data.get('level'), 0),
            'expire_time': raid_end,
            'raid_begin': raid_begin,
            'lat': float(data['latitude']),
            'lng': float(data['longitude'])
        }

        raid['gmaps'] = get_gmaps_link(raid['lat'], raid['lng'])
        raid['applemaps'] = get_applemaps_link(raid['lat'], raid['lng'])

        return raid

    @staticmethod
    def gym(data):
        gym = {
            'type': "gym",
            'id': data.get('gym_id', data.get('id')),
            "team_id": int(data.get('team_id', data.get('team'))),
            "points": str(data.get('gym_points')),
            "guard_pkmn_id": data.get('guard_pokemon_id'),
            'lat': float(data['latitude']),
            'lng': float(data['longitude']),
            'name': check_for_none(str, data.get('name'), 'unknown'),
            'description': check_for_none(str, data.get('description'), 'unknown'),
            'url': check_for_none(str, data.get('url'), 'unknown')
        }
        gym['gmaps'] = get_gmaps_link(gym['lat'], gym['lng'])
        gym['applemaps'] = get_applemaps_link(gym['lat'], gym['lng'])

        return gym


# Ensure that the value isn't None but replacing with a default
def check_for_none(type_, val, default):
    return type_(val) if val is not None else default
