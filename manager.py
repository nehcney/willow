# Standard Library Imports
import os
import logging
from queue import Queue
from itertools import chain
from datetime import datetime
from time import sleep

# 3rd Party Imports
import ujson
from gevent import spawn
from gevent import sleep as g_sleep

# Local Imports
import config
from utils import get_path, get_time_as_str, get_neighborhood, get_distance
from map import Map
from slack import get_name, polite_dm


logger = logging.getLogger('willow')

data_queue = Queue()


class Manager(object):
    def __init__(self, name):
        self.__name = str(name)
        self.__pokemon_name, self.__move_name = {}, {}
        self.__update_pkmn_and_move_names()
        spawn(self.run)
        # logger.debug("Manager '{}' successfully created.".format(name))

    def run(self):
        while True:
            data = data_queue.get(block=True)
            obj = Map.make_object(data)
            try:
                kind = obj['type']
                if kind == "pokemon":
                    self.process_pokemon(obj)
                elif kind == "raid":
                    self.process_raid(obj)
            except TypeError:
                pass
            data_queue.task_done()
            g_sleep(0)

    def process_pokemon(self, pkmn):
        """
        Process new Pokemon data and decide if a notification needs to be sent.

        :param pkmn: pokemon object from webhook
        """
        try:
            pkmn_name = self.__pokemon_name[pkmn['pkmn_id']]
            cp = int(pkmn['cp']) if pkmn['cp'] != '?' else 0
            iv = int("{:.0f}".format(pkmn['iv'])) if pkmn['iv'] != '?' else 0
            atk = int(pkmn['atk']) if pkmn['atk'] != '?' else 0
            def_ = int(pkmn['def']) if pkmn['def'] != '?' else 0
            sta = int(pkmn['sta']) if pkmn['sta'] != '?' else 0
            quick_move = self.__move_name.get(pkmn['quick_id'], 'unknown')
            charge_move = self.__move_name.get(pkmn['charge_id'], 'unknown')
            neighborhood = get_neighborhood(config.NEIGHBORHOODS, pkmn['lat'], pkmn['lng'])

            time_left, _12h_time, _24h_time = get_time_as_str(pkmn['disappear_time'])

            # Early exit situations
            if not config.TRIGGERS:
                logger.warning('No triggers exist!')
                return
            if time_left[0] in ('-', '0'):
                # logger.warning("Pokemon has no time left. Skipping.")
                return

            # Parse the pkmn attributes into a set (4 <= len(set) <= 7)
            input_set = parse_attrib_to_set(pkmn_name, quick_move, charge_move, neighborhood)

            # Find all users that match their criteria against pkmn attributes
            users_dict = {}
            users_with_more_words = {}
            for word in input_set:
                if word not in config.TRIGGERS:
                    continue
                for key, value in config.TRIGGERS[word].items():
                    user_id, _, user_iv, user_cp, user_distance = key
                    if value > 1:
                        try:
                            users_with_more_words[key] -= 1
                            if users_with_more_words[key] > 1:
                                continue
                        except KeyError:
                            users_with_more_words[key] = value
                            continue
                    if user_distance >= 0:
                        pkmn_distance = get_distance(config.USERS[user_id].coord, (pkmn['lat'], pkmn['lng']))
                        if match_iv_cp_distance(user_id, user_iv, iv, user_cp, cp, user_distance, pkmn_distance):
                            users_dict[user_id] = pkmn_distance
                    elif match_iv_cp_distance(user_id, user_iv, iv, user_cp, cp):
                        users_dict[user_id] = None if user_id not in users_dict else users_dict[user_id]

            if not users_dict:
                return

            # DM the appropriate users
            logger.debug(input_set)

            if not cp:
                phrase = (
                    "<{}|{} spawned in {}!> - Available until {} ({})."
                    .format(
                        pkmn['gmaps'], pkmn_name, neighborhood, _12h_time, time_left))
            else:
                phrase = (
                    #  1  2  3             4                      5   6           7         8    9 10 11            12   13
                    "<{}|{}{} spawned in {}!> - Available until {} ({}).\n*CP:* {} (L{})\n*IV:* {}% ({}/{}/{})\n*Moves:* {} / {}"
                    .format(
                        pkmn['gmaps'], pkmn_name, pkmn['gender'], neighborhood, _12h_time, time_left,
                        cp, pkmn['level'], iv, atk, def_, sta, quick_move, charge_move))

            logger.debug("Sending out the notifications!")
            for user_id, distance in users_dict.items():
                distance_msg = "\n*Distance:* {}m".format(round(distance)) if distance else ""
                polite_dm(user_id, phrase + distance_msg, True)
            logger.info("Users notified: {}".format([get_name(user_id) for user_id in users_dict]))
        except Exception as e:
            logger.error("Something fucked -> {}".format(e))

    def process_raid(self, raid):
        """
        Process new raid data and decide if a notification needs to be sent.

        :param raid: raid object from webhook
        """
        raid_id = raid['id']
        raid_end = raid['expire_time']
        pkmn_id = raid['pkmn_id']

        # Check against cache of processed raids
        if raid_id in config.RAID_CACHE:
            prev_raid_end = config.RAID_CACHE[raid_id]['expire_time']
            prev_pkmn_id = config.RAID_CACHE[raid_id].get('pkmn_id', 0)
            if prev_raid_end == raid_end and prev_pkmn_id == pkmn_id:
                # logger.warning("Raid already processed. Skipping. ({})".format(raid_id))
                return
        config.RAID_CACHE[raid_id] = {'expire_time': raid_end, 'pkmn_id': pkmn_id}

        pkmn_name = self.__pokemon_name[pkmn_id] if pkmn_id > 0 else ""
        quick_move = self.__move_name.get(raid['quick_id'], "")
        charge_move = self.__move_name.get(raid['charge_id'], "")
        neighborhood = get_neighborhood(config.NEIGHBORHOODS, raid['lat'], raid['lng'])
        egg = True if not pkmn_name else False
        level = raid['raid_level']

        raid_begin = raid['raid_begin']
        raid_time_left, raid_12h_time, raid_24h_time = get_time_as_str(raid_end)
        egg_time_left, egg_12h_time, egg_24h_time = get_time_as_str(raid_begin)

        # Early exit situations
        if not config.RAID_TRIGGERS:
            logger.warning('No raid triggers exist!')
            return
        if raid_time_left[0] in ('-', '0'):
            logger.warning("Raid has no time left. Skipping.")
            return

        # Parse the raid attributes into a set (4 <= len(set) <= 7)
        input_set = parse_attrib_to_set(pkmn_name, quick_move, charge_move, neighborhood)

        # Find all users that match their criteria against raid attributes
        users_dict = {}
        users_with_more_words = {}
        for word in input_set:
            if word not in config.RAID_TRIGGERS:
                continue
            for key, value in config.RAID_TRIGGERS[word].items():
                user_id, _, user_lvl, user_distance, user_wants_egg = key
                if egg and not user_wants_egg:
                    continue
                if value > 1:
                    try:
                        users_with_more_words[key] -= 1
                        if users_with_more_words[key] > 1:
                            continue
                    except KeyError:
                        users_with_more_words[key] = value
                        continue
                if user_distance >= 0:
                    raid_distance = get_distance(config.USERS[user_id].coord, (raid['lat'], raid['lng']))
                    if match_lvl_distance(user_id, user_lvl, level, user_distance, raid_distance):
                        users_dict[user_id] = raid_distance
                elif match_lvl_distance(user_id, user_lvl, level):
                    users_dict[user_id] = None if user_id not in users_dict else users_dict[user_id]
        # Check level-only entries
        try:
            for key in config.RAID_TRIGGERS['all']:
                user_id, _, user_lvl, user_distance, user_wants_egg = key
                if egg and not user_wants_egg:
                    continue
                if user_distance >= 0:
                    raid_distance = get_distance(config.USERS[user_id].coord, (raid['lat'], raid['lng']))
                    if match_lvl_distance(user_id, user_lvl, level, user_distance, raid_distance):
                        users_dict[user_id] = raid_distance
                elif match_lvl_distance(user_id, user_lvl, level):
                    users_dict[user_id] = None if user_id not in users_dict else users_dict[user_id]
        except KeyError:
            logger.warning("No level-only raid entries. Skipping.")

        if not users_dict:
            return

        # DM the appropriate users
        logger.debug("Raid info -> {}".format(input_set))

        if egg:
            phrase = (
                "<{}|Level {} egg in {}!> - Egg will hatch at {} ({})".format(
                    raid['gmaps'], level, neighborhood, egg_12h_time, egg_time_left))
        else:
            phrase = (
                "<{}|{} raid in {}! (Level {})> - Available until {} ({}).\n*Moves:* {} / {}".format(
                    raid['gmaps'], pkmn_name, neighborhood, level, raid_12h_time, raid_time_left,
                    quick_move, charge_move))

        logger.debug("Sending out the raid notifications!")
        for user_id, distance in users_dict.items():
            distance_msg = "\n*Distance:* {}m".format(round(distance)) if distance else ""
            polite_dm(user_id, phrase + distance_msg)
        logger.info("Users to be notified of raid: {}".format([get_name(user_id) for user_id in users_dict]))

    def __update_pkmn_and_move_names(self):
        """
        Initializes pokemon names and move names.
        """
        locale_path = os.path.join(get_path('en'))
        # Update pokemon names
        with open(os.path.join(locale_path, 'pokemon.json'), 'r') as data:
            names = ujson.loads(data.read())
            for pkmn_id, value in names.items():
                self.__pokemon_name[int(pkmn_id)] = value
        # Update move names
        with open(os.path.join(locale_path, 'moves.json'), 'r') as data:
            moves = ujson.loads(data.read())
            for move_id, value in moves.items():
                self.__move_name[int(move_id)] = value


def parse_attrib_to_set(*args):
    """
    Parse the args into a unique set of individual words.

    :param args: words (eg. "water gun", "van nuys")
    :return: set of (str)
    """
    return {x for x in chain.from_iterable([y.lower().split() for y in args])}


def match_iv_cp_distance(user_id, user_iv, pkmn_iv, user_cp, pkmn_cp, user_distance=None, pkmn_distance=None):
    if config.USERS[user_id].mute:
        return False

    iv_match = (
        user_iv[1] == '+' and pkmn_iv >= user_iv[0] or
        user_iv[1] == '-' and pkmn_iv <= user_iv[0] or
        user_iv[1] == '=' and pkmn_iv == user_iv[0]
    )
    cp_match = (
        user_cp[1] == '+' and pkmn_cp >= user_cp[0] or
        user_cp[1] == '-' and pkmn_cp <= user_cp[0] or
        user_cp[1] == '=' and pkmn_cp == user_cp[0]
    )
    if user_distance:
        return iv_match and cp_match and pkmn_distance <= user_distance

    return iv_match and cp_match


def match_lvl_distance(user_id, user_lvl, raid_lvl, user_distance=None, pkmn_distance=None):
    if config.USERS[user_id].mute:
        return False

    lvl_match = (
        user_lvl[1] == '+' and raid_lvl >= user_lvl[0] or
        user_lvl[1] == '-' and raid_lvl <= user_lvl[0] or
        user_lvl[1] == '=' and raid_lvl == user_lvl[0]
    )

    return lvl_match and pkmn_distance <= user_distance if user_distance else lvl_match


def clean_raid_cache():
    while True:
        # Clean out raid cache every 30 minutes
        sleep(1800)
        logger.debug("Cleaning raid cache...")
        for key, value in list(config.RAID_CACHE.items()):
            if value['expire_time'] < datetime.utcnow():
                del config.RAID_CACHE[key]


def add_managers(num):
    """
    Add additional manager(s). Not to exceed MAX_MANAGERS.
    """
    for _ in range(num):
        num_managers = len(config.MANAGER)
        if num_managers < config.MAX_MANAGERS:
            config.MANAGER.append(Manager(num_managers + 1))
        else:
            logger.warning("MAX_MANAGERS ({}) reached! Cannot add more!".format(config.MAX_MANAGERS))
            return
    logger.info("Successfully created ({}) managers.".format(num))


def add_cleaner():
    logger.info("Adding cleaner...")
    spawn(clean_raid_cache)
