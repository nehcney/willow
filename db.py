# Standard imports
import os
import pickle
import logging
from queue import Queue
from time import sleep

# 3rd party imports
import redis
import ujson
from gevent import spawn

# Local imports
import config


logger = logging.getLogger('willow')

r = redis.from_url(config.REDIS_URL)

triggers_queue = Queue()
raid_triggers_queue = Queue()
directory_queue = Queue()
raid_directory_queue = Queue()


def rexists(key):
    return r.exists(key)


def rset(key, value):
    pickle_dump = pickle.dumps(value, protocol=pickle.HIGHEST_PROTOCOL)
    return r.set(key, pickle_dump)


def rget(key):
    value = r.get(key)
    return pickle.loads(value) if value else None


def rdelete(*key):
    return r.delete(*key)


def rkeys():
    return r.keys()


########################################################################################################################
#                                                   Redis                                                              #
########################################################################################################################

def update_triggers():
    if triggers_queue.qsize() > 0:
        return
    triggers_queue.put(True)


def update_raid_triggers():
    if raid_triggers_queue.qsize() > 0:
        return
    raid_triggers_queue.put(True)


def update_directory():
    if directory_queue.qsize() > 0:
        return
    directory_queue.put(True)


def update_raid_directory():
    if raid_directory_queue.qsize() > 0:
        return
    raid_directory_queue.put(True)


def update_users():
    rset('users', config.USERS)


def update_names():
    rset('names', config.NAMES)


def update_channels():
    rset('channels', config.CHANNELS)


def update_admins():
    rset('admins', config.ADMINS)


def triggers_manager():
    while True:
        triggers_queue.get(block=True)
        rset('triggers', config.TRIGGERS)
        logger.debug('-~~-~~-~~- Updated redis triggers -~~-~~-~~-')
        triggers_queue.task_done()
        sleep(10)


def raid_triggers_manager():
    while True:
        raid_triggers_queue.get(block=True)
        rset('raid_triggers', config.RAID_TRIGGERS)
        logger.debug('-~~-~~-~~- Updated redis raid_triggers -~~-~~-~~-')
        raid_triggers_queue.task_done()
        sleep(10)


def directory_manager():
    while True:
        directory_queue.get(block=True)
        rset('directory', config.DIRECTORY)
        logger.debug('-~~-~~-~~- Updated redis directory -~~-~~-~~-')
        directory_queue.task_done()
        sleep(10)


def raid_directory_manager():
    while True:
        raid_directory_queue.get(block=True)
        rset('raid_directory', config.RAID_DIRECTORY)
        logger.debug('-~~-~~-~~- Updated redis raid_directory -~~-~~-~~-')
        raid_directory_queue.task_done()
        sleep(10)


def add_redis_manager():
    spawn(triggers_manager)
    spawn(raid_triggers_manager)
    spawn(directory_manager)
    spawn(raid_directory_manager)
    logger.debug("Successfully created redis managers.")


def print_database():
    # Output database items
    if not os.path.exists('output'):
        os.makedirs('output')
    t = rget('triggers')
    rt = rget('raid_triggers')
    d = rget('directory')
    rd = rget('raid_directory')
    u = rget('users')
    n = rget('names')
    a = rget('admins')
    c = rget('channels')
    with open('output/triggers.json', 'w') as f:
        ujson.dump(t, f, indent=4)
    with open('output/raid_triggers.json', 'w') as f:
        ujson.dump(rt, f, indent=4)
    with open('output/directory.json', 'w') as f:
        ujson.dump(d, f, indent=4)
    with open('output/raid_directory.json', 'w') as f:
        ujson.dump(rd, f, indent=4)
    with open('output/users.json', 'w') as f:
        ujson.dump(u, f, indent=4)
    with open('output/names.json', 'w') as f:
        ujson.dump(n, f, indent=4)
    with open('output/admins.json', 'w') as f:
        ujson.dump(a, f, indent=4)
    with open('output/channels.json', 'w') as f:
        ujson.dump(c, f, indent=4)
    logger.debug('-~~-~~-~~- Redis database printed! -~~-~~-~~-')
