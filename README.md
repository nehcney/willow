# Willow

Willow is a custom notification system that connects Monocle with Slack. It allows each Slack user to define his or her own custom notifications.