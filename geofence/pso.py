from shapely.geometry import Polygon


ENCINO = Polygon((
    (34.1866007, -118.5271704),     # top left
    (34.1587287, -118.5271543),     # mid left saddle
    (34.1463031, -118.5333556),
    (34.1298349, -118.5396051),     # bot left
    (34.1264512, -118.4758008),     # bot right 405
    (34.1541210, -118.4679620),     # ventura and 405
    (34.1630760, -118.4697000),     # river and 405
    (34.1709170, -118.4775180),     # river and burbank
    (34.1800180, -118.5011040),     # balboa and river
    (34.1866150, -118.5011040)      # top right
))

TARZANA = Polygon((
    (34.1866008, -118.5448600),     # top left
    (34.1328170, -118.5448600),     # bot left
    (34.1298349, -118.5396051),     # bot right
    (34.1463031, -118.5333556),
    (34.1587287, -118.5271543),     # mid left saddle
    (34.1866007, -118.5271704)      # top right
))

RESEDA = Polygon((
    (34.2150000, -118.5448600),     # top left
    (34.1866008, -118.5448600),     # bot left
    (34.1866007, -118.5271704),     # bot right
    (34.2150000, -118.5271704),     # top right
))

LAKE_BALBOA = Polygon((
    (34.2150000, -118.5271704),     # top left
    (34.1866007, -118.5271704),     # bot left
    (34.1866150, -118.5011040),     # balboa and victory
    (34.1800180, -118.5011040),     # balboa and river
    (34.1709170, -118.4775180),     # river and burbank
    (34.1630760, -118.4697000),     # river and 405
    (34.1721400, -118.4672950),     # burbank and 405
    (34.1764370, -118.4679020),
    (34.1794520, -118.4697820),     # 405 and oxnard
    (34.1866150, -118.4748860),     # victory and haskell
    (34.2150000, -118.4748860),     # top right
))

VAN_NUYS = Polygon((
    (34.2150000, -118.4748860),     # top left
    (34.1866150, -118.4748860),     # victory and haskell
    (34.1794520, -118.4697820),     # 405 and oxnard
    (34.1794350, -118.4394940),     # oxnard and hazeltine
    (34.1944170, -118.4394940),     # vanowen and hazeltine
    (34.1944170, -118.4318270),     # vanowen and woodman
    (34.2017860, -118.4318270),     # sherman way and woodman
    (34.2017860, -118.4269550),     # sherman way and river
    (34.2150000, -118.4269550),     # top right
))

SHERMAN_OAKS = Polygon((
    (34.1794520, -118.4697820),     # 405 and oxnard
    (34.1764370, -118.4679020),
    (34.1721400, -118.4672950),     # burbank and 405
    (34.1630760, -118.4697000),     # river and 405
    (34.1541210, -118.4679620),     # ventura and 405
    (34.1264512, -118.4758008),     # bot left
    (34.1232100, -118.4040800),     # bot right
    (34.1404740, -118.4136860),     # mid right saddle
    (34.1721030, -118.4139960),     # burbank and coldwater
    (34.1721030, -118.4394940),     # burbank and hazeltine
    (34.1794350, -118.4394940),     # oxnard and hazeltine
))

VALLEY_GLEN = Polygon((
    (34.1944170, -118.4394940),     # vanowen and hazeltine
    (34.1721030, -118.4394940),     # burbank and hazeltine
    (34.1721030, -118.3892560),     # burbank and 170
    (34.1753060, -118.3929720),     # 170
    (34.1812080, -118.3963410),     # 170
    (34.1860170, -118.4011380),     # 170
    (34.2012330, -118.4035730),     # 170
    (34.2150000, -118.4067450),     # top right
    (34.2150000, -118.4269550),     # top left
    (34.2017860, -118.4269550),     # sherman way and river
    (34.2017860, -118.4318270),     # sherman way and woodman
    (34.1944170, -118.4318270),     # vanowen and woodman
))

VALLEY_VILLAGE = Polygon((
    (34.1721030, -118.4139960),     # burbank and coldwater
    (34.1567240, -118.4139960),     # 101 and coldwater
    (34.1567240, -118.4037370),     # 101 and river
    (34.1544570, -118.3983620),     # 101 and laurel
    (34.1544570, -118.3777530),     # 101 and 170
    (34.1686390, -118.3844430),     # chandler and 170
    (34.1721030, -118.3892560),     # burbank and 170
))

STUDIO_CITY = Polygon((
    (34.1567240, -118.4139960),     # 101 and coldwater
    (34.1404740, -118.4136860),     # mid left
    (34.1232100, -118.4040800),     # bot left
    (34.1274700, -118.3602000),     # mulholland dr
    (34.1315000, -118.3482700),     # bot right
    (34.1361130, -118.3605940),     # 170
    (34.1437840, -118.3674210),     # river and 170
    (34.1544570, -118.3777530),     # 101 and 170
    (34.1544570, -118.3983620),     # 101 and laurel
    (34.1567240, -118.4037370),     # 101 and river
))

NOHO = Polygon((
    (34.2150000, -118.4067450),     # top left
    (34.2012330, -118.4035730),     # 170
    (34.1860170, -118.4011380),     # 170
    (34.1812080, -118.3963410),     # 170
    (34.1753060, -118.3929720),     # 170
    (34.1721030, -118.3892560),     # burbank and 170
    (34.1686390, -118.3844430),     # chandler and 170
    (34.1544570, -118.3777530),     # 101 and 170
    (34.1437840, -118.3674210),     # river and 170
    (34.1361130, -118.3605940),
    (34.1315000, -118.3482700),     # bot right
    (34.1425330, -118.3452310),     # right of noho and toluca
    (34.1432420, -118.3613730),     # river and lankershim
    (34.1576950, -118.3702170),     # camarillo and vineland
    (34.1594710, -118.3702170),     # huston and vineland
    (34.1594710, -118.3614940),     # huston and cahuenga
    (34.1649130, -118.3614940),     # magnolia and cahuenga
    (34.1649130, -118.3547240),     # magnolia and clybourn
    (34.2150000, -118.3547240),     # top right
))

TOLUCA_LAKE = Polygon((
    (34.1594710, -118.3702170),     # huston and vineland
    (34.1576950, -118.3702170),     # camarillo and vineland
    (34.1432420, -118.3613730),     # river and lankershim
    (34.1425330, -118.3452310),     # bot right
    (34.1649130, -118.3547240),     # magnolia and clybourn
    (34.1649130, -118.3614940),     # magnolia and cahuenga
    (34.1594710, -118.3614940),     # huston and cahuenga
))

NEIGHBORHOODS = {
    'Encino': ENCINO,
    'Tarzana': TARZANA,
    'Reseda': RESEDA,
    'Lake Balboa': LAKE_BALBOA,
    'Van Nuys': VAN_NUYS,
    'Sherman Oaks': SHERMAN_OAKS,
    'Valley Glen': VALLEY_GLEN,
    'Valley Village': VALLEY_VILLAGE,
    'Studio City': STUDIO_CITY,
    'NoHo': NOHO,
    'Toluca Lake': TOLUCA_LAKE
}
