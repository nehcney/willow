# Monkey Patch to allow Gevent's Concurrency
from gevent import pywsgi, spawn, monkey
monkey.patch_all()

# Standard imports
import ujson
import logging
from flask import Flask, request, abort
from sys import exit

# 3rd party imports

# Local imports
import db
import manager
import slack
import config
from config import HOST_URL, HOST_PORT, ADMINS, OWNER
from log import setup_logging


setup_logging()
logger = logging.getLogger('willow')

app = Flask(__name__)


########################################################################################################################
#                                                   Webhook                                                            #
########################################################################################################################

@app.route('/', methods=['GET'])
def index():
    return "Willow Running!"


@app.route('/', methods=['POST'])
def accept_webhook():
    try:
        # logger.debug("POST request received from {}.".format(request.remote_addr))
        data = ujson.loads(request.data)
        manager.data_queue.put(data)
        if manager.data_queue.qsize() > 500:
            logger.error("Queue length: {}! Exit as a fail safe.".format(manager.data_queue.qsize()))
            for key in ADMINS:
                slack.__dm(key, "*ERROR* - Willow has crashed due to data_queue buildup!")
            exit(1)
        if manager.data_queue.qsize() > 20:
            logger.info("Queue length is at {}, adding more managers.".format(manager.data_queue.qsize()))
            manager.add_managers(1)
    except Exception as e:
        logger.error("Encountered error while receiving webhook ({}: {})".format(type(e).__name__, e))
        abort(400)
    return "OK"  # request ok


def start_server():
    """
    Start up Server
    """
    logger.info("Listening for webhooks on: {}:{}".format(HOST_URL, HOST_PORT))
    server = pywsgi.WSGIServer((HOST_URL, HOST_PORT), app)
    spawn(server.serve_forever)


########################################################################################################################
#                                                     Init                                                             #
########################################################################################################################

def initialize():
    config.TRIGGERS = db.rget('triggers') if db.rexists('triggers') else {}
    config.RAID_TRIGGERS = db.rget('raid_triggers') if db.rexists('raid_triggers') else {}
    config.DIRECTORY = db.rget('directory') if db.rexists('directory') else {}
    config.RAID_DIRECTORY = db.rget('raid_directory') if db.rexists('raid_directory') else {}
    config.USERS = db.rget('users') if db.rexists('users') else {}
    config.NAMES = db.rget('names') if db.rexists('names') else {}
    config.ADMINS = db.rget('admins') if db.rexists('admins') else {}
    config.CHANNELS = db.rget('channels') if db.rexists('channels') else []


########################################################################################################################
#                                                     Main                                                             #
########################################################################################################################

if __name__ == '__main__':
    initialize()

    # Always add owner to admin list
    slack.__add_admin(slack.get_id(OWNER))

    # Run willow!
    manager.add_managers(2)
    manager.add_cleaner()
    slack.add_dm_manager(10)
    db.add_redis_manager()
    start_server()
    slack.start_slack_manager()
