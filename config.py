import logging
from slackclient import SlackClient
from geofence import pso, bag, sc, nh, wh
from utils import populate_dictionary

logger = logging.getLogger('willow')


########################################################################################################################
#                                                    Config                                                            #
########################################################################################################################

#   CHANGE THE BELOW!   #
# -~=~-~=~-~=~-~=~-~=~- #
SLACK = 'yentest'
OWNER = 'yen_chen'
# -~=~-~=~-~=~-~=~-~=~- #

HOST_URL = '127.0.0.1'
HOST_PORT = 4000
REDIS_URL = 'redis://127.0.0.1:6379'

MAX_MANAGERS = 10
MAX_DM_MANAGERS = 50
MAX_ALERTS = 100
MAX_RAID_ALERTS = 50
MAX_NOTIFICATION_QUEUE = 10

READ_WEBSOCKET_DELAY = .1
DM_BUFFER = 5

########################################################################################################################
#                                                    Globals                                                           #
########################################################################################################################

TRIGGERS = {}
RAID_TRIGGERS = {}
DIRECTORY = {}
RAID_DIRECTORY = {}
USERS = {}
NAMES = {}
ADMINS = {}
CHANNELS = []
MANAGER = []
DMS = 0
RAID_CACHE = {}

SLACK_DICT = {
    'pokeshermanoaks': {
        'token': 'xoxb-187458698182-9rra6OD8yCxx0fmLwRoNcoUT',
        'id': 'U5HDGLJ5C',
        'geofence': pso.NEIGHBORHOODS,
        'website': 'http://pokeshermanoaks.tk'
    },
    'bagpokes': {
        'token': 'xoxb-192454814358-ZKIlkBR1I5noNCJV7ujEREIb',
        'id': 'U5NDCPYAJ',
        'geofence': bag.NEIGHBORHOODS,
        'website': 'http://bagpokes.tk'
    },
    'santaclaritamon': {
        'token': 'xoxb-222611527239-PHiq4RnV16v22l1Cu04Gecgk',
        'id': 'U6JHZFH71',
        'geofence': sc.NEIGHBORHOODS,
        'website': 'http://santaclaritamon.tk'
    },
    'pokenorthhills': {
        'token': 'xoxb-274513876690-VUcWwP97K4qZIs93G0iO1tuN',
        'id': 'U82F3RSLA',
        'geofence': nh.NEIGHBORHOODS,
        'website': 'http://pokenorthhills.tk'
    },
    'pokewoodlandhills': {
        'token': 'xoxb-274566715060-kL1Zu3oq1AzQlOFxrJlGYZ0E',
        'id': 'U5E0FAW4F',
        'geofence': wh.NEIGHBORHOODS,
        'website': 'http://pokewoodlandhills.tk'
    },
    'yentest': {
        'token': 'xoxb-184015370151-cj9p4sHqC7K9n5Hzdf8QbOO8',
        'id': 'U5E0FAW4F',
        'geofence': pso.NEIGHBORHOODS,
        'website': 'http://pokeshermanoaks.tk'
    }
}

SLACK_BOT_TOKEN = SLACK_DICT[SLACK]['token']
BOT_ID = SLACK_DICT[SLACK]['id']
slack_client = SlackClient(SLACK_BOT_TOKEN)

NEIGHBORHOODS = SLACK_DICT[SLACK]['geofence']
WEBSITE = SLACK_DICT[SLACK]['website']

DICTIONARY, NEIGHBORHOODS_SPLIT = populate_dictionary(NEIGHBORHOODS)
