# Standard Library Imports
import re
import logging
from sys import exit
from queue import Queue
from time import sleep
from random import randint

# 3rd party libraries
from gevent import spawn
from gevent import sleep as g_sleep
from websocket import _exceptions

# Local libraries
import config
from config import slack_client
import msg
from user import SlackUser
from commands import parse_location, add_notification, add_raid_notification, remove, list_directory, mute
from db import (
    update_triggers, update_directory, update_channels, update_names, update_users, update_admins, print_database
)


logger = logging.getLogger('willow')


# Constants
notification_queue = {}
dm_queue = Queue()


########################################################################################################################
#                                                Process Input                                                         #
########################################################################################################################


def parse_slack_output(slack_rtm_output):
    """
    The Slack Real Time Messaging API is an events firehose. This parsing function returns None unless a message is
    directed at the Bot, based on its ID. In the rare event 'team_migration_started', returns (1, None, None).

    :return: (user_id, text, channel)
    """
    output_list = slack_rtm_output
    if output_list:
        # logger.debug("Incoming Slack event: {}".format(output_list))
        for output in output_list:
            try:
                # handle server migration
                if output['type'] == 'team_migration_started':
                    return 1, None, None

                # handle new user by updating db
                elif output['type'] == 'team_join':
                    name = output['user']['name']
                    logger.info("A new user has joined Slack! Welcome, {}!".format(name))
                    user_id = output['user']['id']
                    try:
                        logger.debug("Grabbing {}'s DM channel in order to update database.".format(name))
                        ret = slack_client.api_call("im.open", user=user_id)
                        if not ret['ok']:
                            logger.error("Could not open DM channel with {}! -> {}".format(name, ret['error']))
                            logger.warning("Could not add {} to database!".format(name))
                            continue
                        channel = ret['channel']['id']
                    except Exception as e:
                        logger.error("Exception during 'slack_client.api_call' (im.open) -> {}".format(e))
                        logger.warning("Could not add {} to database!".format(name))
                        continue
                    config.USERS[user_id] = SlackUser(name, channel)
                    config.NAMES[name] = user_id
                    update_users()
                    update_names()
                    logger.debug("Added {} to our database.".format(name))
                    dm(user_id, msg.INTRO)

                # handle user change
                elif output['type'] == 'user_change':
                    user_id = output['user']['id']
                    if config.USERS.get(user_id):
                        old_name = config.USERS[user_id].name
                        new_name = output['user']['name']

                        # handle user changes nickname
                        if old_name != new_name:
                            logger.info("{} changed his/her nickname to {}! Updating database.".format(old_name, new_name))
                            config.USERS[user_id].name = new_name
                            del config.NAMES[old_name]
                            config.NAMES[new_name] = user_id
                            update_users()
                            update_names()
                            logger.debug("USERS and NAMES updated in database.")

                        # handle user deleted
                        if output['user']['deleted']:
                            logger.info("{}'s Slack account has been deactivated! Removing from database.".format(new_name))
                            remove(user_id, new_name, ['all'])
                            remove(user_id, new_name, ['all'], raid=True)
                            try:
                                del config.DIRECTORY[user_id]
                                del config.RAID_DIRECTORY[user_id]
                            except KeyError:
                                pass
                            del config.USERS[user_id]
                            del config.NAMES[new_name]
                            update_triggers()
                            update_directory()
                            update_users()
                            update_names()
                            logger.debug("Database purged of all entries pertaining to {}.".format(new_name))

                # handle willow added to channel
                elif output['type'] == 'member_joined_channel' and output['user'] == config.BOT_ID:
                    logger.info('willow invited to channel: ({}). Adding to blocked channel list.'.format(
                        output['channel']))
                    try:
                        config.CHANNELS.append(output['channel'])
                    except AttributeError:  # first time adding a channel
                        config.CHANNELS = [output['channel']]
                    update_channels()
                    logger.debug("Blocked channels list: {}".format(config.CHANNELS))

                # handle direct messages
                elif output['type'] == 'message' and (
                        not output.get('bot_id') and not output.get('dnd_suppressed') and not output.get('subtype')):
                    return output['user'], output['text'].strip(), output['channel']
            except KeyError:
                logger.error("Slack event is missing key 'type'! Skipping.")

    return None, None, None


def handle_command(user_id, text, channel):
    """
    Receives commands directed at the bot and determines if they are valid commands. If so, then acts on the commands.
    If not, returns back what it needs for clarification.

    :param user_id: user's slack id
    :param text: a stripped text string
    :param channel: user's dm channel
    """
    # Handle requests that aren't from a DM
    if config.CHANNELS and channel in config.CHANNELS:
        logger.warning("Command coming from a blocked channel. Ignoring command.")
        return

    name = get_name(user_id, channel)
    if not name:
        dm(user_id, msg.ERROR)
        return

    # Partition command vs. arguments
    logger.info("{} typed: {}".format(name, text))
    cmd, _, args = text.partition(' ')
    cmd = cmd.lower()
    args = args.strip()

    # Handle location
    if cmd == 'location':
        dm(user_id, parse_location(user_id, name, args))
        return

    # Handle alerts
    if cmd in ('notify', 'alert'):
        logger.debug("{} wants to add an alert.".format(name))
        alerts = list(filter(None, args.split(',')))
        dm(user_id, add_notification(user_id, name, alerts))
        return

    # Handle removes
    if cmd in ('remove', 'delete'):
        args = list(filter(None, re.split('[, ]', args)))
        logger.debug("{} wants to remove {}".format(name, args))
        dm(user_id, remove(user_id, name, args))
        return

    # Handle list
    if cmd == 'list':
        logger.debug("{} wants to view his/her list.".format(name))
        dm(user_id, list_directory(user_id, name))
        return

    # Handle raid
    if cmd == 'raid':

        if not args:
            logger.debug("{} wants to view the raid help message.".format(name))
            dm(user_id, msg.RAID_HELP)
            return

        else:
            raid_cmd, _, raid_args = args.partition(' ')
            raid_cmd = raid_cmd.lower()

            # Handle raid alerts
            if raid_cmd in ('notify', 'alert'):
                logger.debug("{} wants to add a raid alert.".format(name))
                alerts = list(filter(None, raid_args.split(',')))
                dm(user_id, add_raid_notification(user_id, name, alerts))
                return

            # Handle raid removes
            if raid_cmd in ('remove', 'delete'):
                args = list(filter(None, re.split('[, ]', raid_args)))
                logger.debug("{} wants to remove raid {}".format(name, args))
                dm(user_id, remove(user_id, name, args, True))
                return

            # Handle raid list
            if raid_cmd == 'list':
                logger.debug("{} wants to view his/her raid list.".format(name))
                dm(user_id, list_directory(user_id, name, True))
                return

            # Handle raid help
            if raid_cmd == 'help':
                logger.debug("{} wants to view the raid help message.".format(name))
                dm(user_id, msg.RAID_HELP)
                return

    # Handle mute
    if cmd == 'mute':
        logger.debug("{} wants to mute all alerts.".format(name))
        dm(user_id, mute(user_id, name))
        return

    # Handle unmute
    if cmd == 'unmute':
        logger.debug("{} wants to unmute all alerts.".format(name))
        dm(user_id, mute(user_id, name, False))
        return

    # Handle help
    if cmd == 'help':
        logger.debug("{} wants to view the help message.".format(name))
        dm(user_id, msg.HELP)
        return

    # Handle admin commands
    if cmd == 'admin':

        # Check if admin
        if not is_admin(user_id):
            logger.warning("{} tried (unsuccessfully) to access an admin command!".format(name))
            dm(user_id, msg.NOT_ADMIN)
            return

        if not args:
            logger.debug("{} wants to view the admin help message.".format(name))
            dm(user_id, msg.ADMIN_HELP)
            return

        else:
            admin_cmd, _, admin_args = args.partition(' ')
            admin_cmd = admin_cmd.lower()

            # admin location
            if admin_cmd == 'location':
                target_name, _, args = admin_args.strip().partition(' ')
                target_name = target_name.lower()
                args = args.strip()
                logger.debug("{} wants to use a location command for {}.".format(name, target_name))
                target_id = get_id(target_name)
                if target_id:
                    dm(user_id, parse_location(target_id, target_name, args))
                else:
                    dm(user_id, "{} not found.".format(target_name))
                return

            # admin notify/alert <name> <alert>
            if admin_cmd in ('notify', 'alert') and admin_args.strip():
                target_name, _, alerts = admin_args.strip().partition(' ')
                target_name = target_name.lower()
                alerts = list(filter(None, alerts.split(',')))
                logger.debug("{} wants to add {} for {}.".format(name, alerts, target_name))
                target_id = get_id(target_name)
                if target_id:
                    dm(user_id, add_notification(target_id, target_name, alerts, override=True))
                else:
                    dm(user_id, "{} not found.".format(target_name))
                return

            # admin remove <name> <num/all>
            if admin_cmd in ('remove', 'delete') and admin_args.strip():
                target_name, _, args = admin_args.strip().partition(' ')
                target_name = target_name.lower()
                args = list(filter(None, re.split('[, ]', args)))
                logger.debug("{} wants to remove {} for {}.".format(name, args, target_name))
                target_id = get_id(target_name)
                if target_id:
                    dm(user_id, remove(target_id, target_name, args))
                else:
                    dm(user_id, "{} not found.".format(target_name))
                return

            # admin list <name>
            if admin_cmd == 'list' and admin_args.strip():
                target_name = admin_args.strip().lower()
                logger.debug("{} wants to view {}'s list.".format(name, target_name))
                target_id = get_id(target_name)
                if target_id:
                    dm(user_id, list_directory(target_id, target_name))
                else:
                    dm(user_id, "{} not found.".format(target_name))
                return

            # Handle raid
            if admin_cmd == 'raid':

                if not admin_args.strip():
                    logger.debug("{} wants to view the raid help message.".format(name))
                    dm(user_id, msg.RAID_HELP)
                    return

                else:
                    admin_args = admin_args.strip()
                    raid_cmd, _, args = admin_args.partition(' ')
                    raid_cmd = raid_cmd.lower()

                    # Handle raid alerts
                    if raid_cmd in ('notify', 'alert'):
                        target_name, _, raid_args = args.strip().partition(' ')
                        target_name = target_name.lower()
                        logger.debug("{} wants to add a raid alert for {}.".format(name, target_name))
                        alerts = list(filter(None, raid_args.split(',')))
                        target_id = get_id(target_name)
                        if target_id:
                            dm(user_id, add_raid_notification(target_id, target_name, alerts, True))
                        else:
                            dm(user_id, "{} not found.".format(target_name))
                        return

                    # Handle raid removes
                    if raid_cmd in ('remove', 'delete'):
                        target_name, _, raid_args = args.strip().partition(' ')
                        target_name = target_name.lower()
                        args = list(filter(None, re.split('[, ]', raid_args)))
                        logger.debug("{} wants to remove raid {} for {}.".format(name, args, target_name))
                        target_id = get_id(target_name)
                        if target_id:
                            dm(user_id, remove(target_id, target_name, args, True))
                        else:
                            dm(user_id, "{} not found.".format(target_name))
                        return

                    # Handle raid list
                    if raid_cmd == 'list':
                        target_name = args.strip().lower()
                        logger.debug("{} wants to view {}'s raid list.".format(name, target_name))
                        target_id = get_id(target_name)
                        if target_id:
                            dm(user_id, list_directory(target_id, target_name, True))
                        else:
                            dm(user_id, "{} not found.".format(target_name))
                        return

                    # Handle raid help
                    if raid_cmd == 'help':
                        logger.debug("{} wants to view the raid help message.".format(name))
                        dm(user_id, msg.RAID_HELP)
                        return

            # admin mute <name>
            if admin_cmd == 'mute' and admin_args.strip():
                target_name = admin_args.strip().lower()
                logger.debug("{} wants to mute {}'s alerts.".format(name, target_name))
                target_id = get_id(target_name)
                if target_id:
                    dm(user_id, mute(target_id, target_name))
                else:
                    dm(user_id, "{} not found.".format(target_name))
                return

            # admin unmute <name>
            if admin_cmd == 'unmute' and admin_args.strip():
                target_name = admin_args.strip().lower()
                logger.debug("{} wants to unmute {}'s alerts.".format(name, target_name))
                target_id = get_id(target_name)
                if target_id:
                    dm(user_id, mute(target_id, target_name, False))
                else:
                    dm(user_id, "{} not found.".format(target_name))
                return

            # admin help
            if admin_cmd == 'help':
                logger.debug("{} wants to view the admin help message.".format(name))
                dm(user_id, msg.ADMIN_HELP)
                return

            # admin print
            if admin_cmd == 'print':
                logger.debug("{} wants to print out the database.".format(name))
                spawn(print_database)
                dm(user_id, "Database items have been printed!")
                return

            # admin dm <name> <message>
            if admin_cmd == 'dm':
                target_name, _, message = admin_args.strip().partition(' ')
                target_name = target_name.lower()
                target_id = get_id(target_name)
                if message.startswith('link'):
                    _, _, message = message.partition(' ')
                    message = message.replace('&lt;', '<')
                    message = message.replace('&gt;', '>')
                logger.debug("{} wants to dm {} a message as willow -> {}".format(name, target_name, message))
                dm(target_id, message)
                dm(user_id, "Message sent!")
                return

    # Unknown command
    dm(user_id, msg.WHAT[randint(0, 11)] + '\n' + msg.COMMANDS)


def get_name(user_id, channel=None):
    try:
        return config.USERS[user_id].name
    except KeyError:
        logger.warning("Could not find name for ({}). Mining 'users.info' for data.".format(user_id))
        try:
            ret = slack_client.api_call("users.info", user=user_id)
            if not ret['ok']:
                logger.error("Failed to get user info for ({})! -> {}".format(user_id, ret['error']))
                logger.warning("Could not get name. Returning None.")
                return None
            name = ret['user']['name']
        except Exception as e:
            logger.error("Exception during 'slack_client.api_call' (users.info) -> {}".format(e))
            logger.warning("Could not get name. Returning None.")
            return None
        if not channel:  # THIS SHOULD NEVER HIT!
            logger.error("Logic dictates that this should never have hit. Oopsies?")
            try:
                ret = slack_client.api_call("im.open", user=user_id)
                if not ret['ok']:
                    logger.error("Could not open DM channel with {}! -> {}".format(name, ret['error']))
                    logger.warning("Could not get DM channel id.")
                    return None
                channel = ret['channel']['id']
            except Exception as e:
                logger.error("Exception during 'slack_client.api_call' (im.open) -> {}".format(e))
                logger.warning("Could not get DM channel id.")
                return None
        config.USERS[user_id] = SlackUser(name, channel)
        config.NAMES[name] = user_id
        update_users()
        update_names()
        logger.debug("Updated USERS and NAMES in database ({})".format(name))
        return name


def get_id(name):
    if config.NAMES.get(name):
        return config.NAMES.get(name)
    else:
        logger.warning("Could not find user id for ({}). Mining 'users.info' for data.".format(name))
        try:
            ret = slack_client.api_call("users.list")
            if not ret['ok']:
                logger.error("Failed to get user list from Slack! -> {}".format(ret['error']))
                logger.warning("Could not get {}'s user id.".format(name))
                return None
            members = ret['members']
            for user in members:
                if user['name'] == name:
                    logger.debug("{}'s id has been found: ({})".format(name, user.get('id')))
                    return user.get('id')
        except Exception as e:
            logger.error("Exception during 'slack_client.api_call' (users.list) -> {}".format(e))
            logger.warning("Could not get {}'s user id.".format(name))
            return None


def get_dm_channel(user_id):
    if config.USERS.get(user_id):
        return config.USERS[user_id].channel
    else:
        logger.warning("Could not find dm channel for ({}). Mining 'users.info' for data.".format(user_id))
        try:
            ret = slack_client.api_call("im.open", user=user_id)
            if not ret['ok']:
                logger.error("Could not open DM channel with ({})! -> {}".format(user_id, ret['error']))
                logger.warning("Could not get DM channel id.")
                return None
            channel = ret['channel']['id']
        except Exception as e:
            logger.error("Exception during 'slack_client.api_call' (im.open) -> {}".format(e))
            logger.warning("Could not get DM channel id.")
            return None
        name = get_name(user_id, channel)
        config.USERS[user_id] = SlackUser(name, channel)
        config.NAMES[name] = user_id
        update_users()
        update_names()
        logger.debug("Updated USERS and NAMES in database ({})".format(name))
        return channel


def is_admin(user_id):
    return config.ADMINS.get(user_id)


def __add_admin(user_id):
    logger.debug("Adding new admin: ({})".format(user_id))
    config.ADMINS[user_id] = True
    update_admins()


def __remove_admin(user_id):
    logger.debug("Removing admin: ({})".format(user_id))
    del config.ADMINS[user_id]
    update_admins()


########################################################################################################################
#                                               Direct Messaging                                                       #
########################################################################################################################


def dm_manager(name):
    while True:
        # if dm_queue.qsize() > 1:
        #     logger.warning("DM queue over ({})! Adding an additional DM manager.".format(dm_queue.qsize()))
        #     add_dm_manager(1)
        # logger.debug("---=== DM queue size: ({}), Num DM managers: ({}) ===---".format(dm_queue.qsize(), config.DMS))
        obj = dm_queue.get(block=True)
        qsize = dm_queue.qsize()
        if qsize > 100:
            logger.debug("---=== DM manager: {}/{} | DM queue size: ({}) ===---".format(name, config.DMS, qsize))
        __dm(obj[0], obj[1])
        dm_queue.task_done()
        g_sleep(0)


def add_dm_manager(num):
    for each in range(num):
        if config.DMS < config.MAX_DM_MANAGERS:
            spawn(dm_manager, each)
            config.DMS += 1
        else:
            logger.warning("MAX_DM_MANAGERS ({}) reached! Cannot add more!".format(config.MAX_DM_MANAGERS))
            return
    logger.info("Successfully added ({}) DM managers. Total DM managers: ({})".format(num, config.DMS))


def dm(user_id, message, pkmn=False):
    if pkmn:
        # Create a notification queue for user if needed
        if not notification_queue.get(user_id):
            notification_queue[user_id] = Queue()
            logger.debug("---=== New notification queue created for: {} ===---".format(get_name(user_id)))
            spawn(notification_queue_manager, user_id)

        # If user's notification queue is > MAX_PKMN_DM, warn and mute the user
        if notification_queue[user_id].qsize() > config.MAX_NOTIFICATION_QUEUE:
            logger.warning("---=== {}'s queue is over MAX! disabling notifications! ===---".format(get_name(user_id)))
            config.USERS[user_id].mute = True
            __dm(user_id, msg.THROTTLED)  # bypass dm queue
            update_users()
            return

        # All's well, add pkmn notification to user's queue
        obj = [user_id, message]
        notification_queue[user_id].put(obj)

    else:  # not a pkmn notification
        obj = [user_id, message]
        dm_queue.put(obj)


def notification_queue_manager(user_id):
    while True:
        if config.USERS[user_id].mute and not notification_queue[user_id].empty():
            logger.info("---=== Clearing {}'s notification queue ===---".format(get_name(user_id)))
            notification_queue[user_id].queue.clear()
        obj = notification_queue[user_id].get(block=True)
        dm_queue.put(obj)
        notification_queue[user_id].task_done()
        sleep(config.DM_BUFFER)


def polite_dm(user_id, message, pkmn=False):
    if not config.USERS[user_id].mute:
        dm(user_id, message, pkmn)
    else:
        logger.debug("{} is muted: alert not sent.".format(get_name(user_id)))


def __dm(user_id, message):
    for i in range(1, 4):
        try:
            ret = slack_client.api_call("chat.postMessage", channel=get_dm_channel(user_id), text=message, as_user=True)
            if ret['ok']:
                break
            logger.error("(chat.postMessage) returned an error! -> {}".format(ret['error']))
        except Exception as e:
            logger.error("Exception during 'slack_client.api_call' (chat.postMessage) -> {}".format(e))
        logger.error("Failed to DM ({}) on try ({})".format(get_name(user_id), i))
        sleep(2)


########################################################################################################################
#                                                   Start                                                              #
########################################################################################################################


def start_slack_manager():
    """
    Slack manager. Connects to and grabs input from slack continuously. If disconnected/unable to connect:
    1. Tries to reconnect every 3 seconds, for 3 tries
    2. If still unable to connect, timeout for 60 seconds
    3. Repeats 1 and 2 for a total of 3 tries. If still can't connect, forces an exit.
    """
    lives = 3
    retries = 3
    while retries > 0:
        if slack_client.rtm_connect():
            logger.info("Willow connected and running!")
            # -~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~ #
            while True:
                try:
                    user, text, channel = parse_slack_output(slack_client.rtm_read())
                    if user == 1:
                        logger.warning("team_migration_started event detected!")
                        break
                    if user and text and channel:
                        handle_command(user, text, channel)
                    sleep(config.READ_WEBSOCKET_DELAY)
                except _exceptions.WebSocketConnectionClosedException as e:
                    logger.error("Websocket exception: {}".format(e))
                    break
            # -~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~ #
            logger.warning("Willow disconnected as a fail safe.")
        else:
            logger.warning("Connection failed on attempt #{}.".format(4 - retries))
            retries -= 1
            if not retries:
                lives -= 1
                if not lives:
                    logger.error("Unable to connect. As a failsafe, Willow will not try to connect again. Exiting.")
                    exit(1)
                logger.info("Unable to connect. Waiting for a full minute before next attempt ({})".format(4 - lives))
                retries = 3
                sleep(60)
            else:
                logger.info("Retrying in 3 seconds...")
                sleep(3)
