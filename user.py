
class SlackUser:
    def __init__(self, name, channel, mute=False, coord=None, loc="", locations=None):
        self.name = name
        self.channel = channel
        self.mute = mute
        self.coord = coord
        self.loc = loc
        self.locations = locations if locations else {}


class NewDirectoryEntry:
    def __init__(self, iv=None, cp=None, distance=-1):
        self.iv = iv if iv else (-1, '+')
        self.cp = cp if cp else (-1, '+')
        self.distance = distance


class RaidDirectoryEntry:
    def __init__(self, lvl=None, distance=-1, egg=False):
        self.lvl = lvl if lvl else (-1, '+')
        self.distance = distance
        self.egg = egg


class NewSlackUser:
    def __init__(self, name, channel, mute=False, loc=None):
        self.name = name
        self.channel = channel
        self.mute = mute
        self.loc = loc
