from config import (
    MAX_ALERTS as MAX,
    MAX_RAID_ALERTS as RMAX,
    OWNER,
    WEBSITE
)


COMMANDS = ">Type `help` for a list of valid commands."
NONE = "You do not have any notifications."
NONE_RAID = "You do not have any raid notifications."
MISSING = "Eh? Notify you of what?"
REMOVED = "Notification(s) removed: "
REMOVED_RAID = "Raid notification(s) removed: "
CLEARED = "All notifications have been removed."
CLEARED_RAID = "All raid notifications have been removed."
MUTE = "Muting all notifications. To unmute, type `unmute`."
UNMUTE = "Unmuting all notifications. To mute, type `mute`."
ERROR = "Oops...something went wrong. Please let *{}* know how naughty I've been.".format(OWNER)
INVALID = ">Invalid alert: "
NOT_ADMIN = "You don't have permission to do that."
MISSING_WORDS = "No words found in your alert!"
MISSPELLED_WORD = "This word is misspelled. Check your spelling and try again."
NUM_ONLY = "You need to supplement IV/CP/distance alerts with a pokemon, city, or attack move."
INVALID_EGG = "Eggs don't have names or attack moves, you silly goose!"
DISTANCE_ONLY = "You need to supplement distance alerts with a pokemon, city, attack move, or raid level."
IV_RANGE = "IV must be between 0-100."
LVL_RANGE = "Level must be between 1-5."
NO_LOC = "You must first set your coordinates before adding distance rules."
LOC_USAGE = "To set your current coordinates, type `location 12.345, -67.890`"
LOC_SAVE = "To save coordinates to a location name, type `location name 12.345, -67.890`"
LOC_LOAD = "To load a saved location, type `location name`"
WEB_TOOL = "For coordinates, neighborhoods, and distance radius visualization, visit {}".format(WEBSITE)
LOC_MORE_HELP = "For additional help, and to view your current and saved locations, type `location`"
OOB = "That notification number does not exist! Type `list` to view your alerts."
OOB_RAID = "That notification number does not exist! Type `raid list` to view your raid alerts."
OVER_MAX = "Adding the alert(s) exceeds your limit of {})! Please make room before adding another.".format(MAX)
OVER_RMAX = "Adding the alert(s) exceeds your raid limit of {})! Please make room before adding another.".format(RMAX)
ALERT_USAGE = "Usage: `notify abra 90+`, `alert muk 45cp-`, `notify encino 3000cp, snorlax iv90+, eevee cp0=`"
REMOVE_USAGE = "To remove alerts: `remove 1,2,3` / `remove 1-10` / `remove 9+` / `remove 5-` / `remove all`"
REMOVE_RAID_USAGE = "To remove raid alerts: `raid remove 1,2,3` / `raid remove 1-10` / `raid remove 9+` / `remove all`"
RAID_ALERT_USAGE = "Usage: `raid notify snorlax 1000m`, `raid notify encino, level4 egg, lapras blizzard`"
THROTTLED = (
    ">>> *!!! WARNING !!!*\nYour notification filters are too general, resulting in too many notifications being sent! "
    "As a safety measure, your notifications have been *disabled*. Please adjust your alerts to be more restrictive, "
    "then type `unmute` to re-enable notifications."
)
WHAT = [
    "I don't understand what you're trying to do.",
    "Me no comprende.",
    "Has anyone really been far even as decided to use even go want to do look more like?",
    "You seem to be typing gibberish.",
    "Words are hard.",
    "Uh...yes. I mean no. Sorry, what?",
    "One of these day's you'll finally get it right.",
    "Confucius say, you no make sense.",
    "Roses are red, violets are blue, you don't make sense, what do I do?",
    ":neutral_face:......?",
    "Sure, I'll get right on tha- *NO THAT MAKES NO SENSE!!!*",
    ":robot_face: `DOES` :robot_face: `NOT` :robot_face: `COMPUTE` :robot_face: `CIRCUITS`:robot_face: `OVERLOADING` "
    ":robot_face:\n:fire::fire::fire::fire::fire::fire::fire::fire::fire::fire::fire::fire::fire::fire::fire::fire:"
    ":fire::fire::fire:"
]

########################################################################################################################

INTRO = (
    ":sparkles: *Hello trainer!* :sparkles:\nAllow me to introduce myself. I am Professor Willow, a bot created after "
    "the legend himself. However, I am capable of _much more_ than my human counterpart: I can alert you to Pokemon "
    "that fit any criteria of your choosing! In other words, *I am a fully customizable personal alert system*. Wow!\n"
    "\n>To get started, simply type `help` in this chat window."
)

########################################################################################################################

HELP = (
    ":point_right: _For help with raids, type `raid` or `raid help`_\n"
    "*Getting Started*\n>Here are some simple commands for Pokemon spawns:\n"
    ">`list` - View all of your alerts.\n"
    ">`location` - View your set location (required to use distance feature)\n"
    ">`notify dragonite` - Add an alert for any Dragonite that spawns. Capitalization does not matter.\n"
    ">`notify snorlax 90iv+ 45cp-` - Add an alert for all Snorlax with 90 IV or above _and_ 45 CP or below.\n"
    ">`notify pidgey 100m` - Add an alert for all Pidgey within 100 meters of your set location.\n"
    ">`notify noho muk` - Add an alert for all Muk in NoHo.\n"
    ">`notify muk, snorlax, 100 eevee` - Add 3 alerts: any Muk, any Snorlax, and all 100 IV Eevee.\n"
    ">`remove 1, 3-7, 15+` - Remove alert(s) based on the listed order number.\n"
    ">`remove all` - Clear all alerts.\n"
    ">`mute` - Temporarily mute all notifications from me.\n"
    ">`unmute` - Resume all notifications from me.\n"
    ">`raid` or `raid help` - :sparkles:*NEW*:sparkles: View the help screen for raids.\n"
    "*Important Things to Note*\n"
    ">• Each trainer is only allowed a maximum of {} pokemon alerts and {} raid alerts.\n"
    ">• For pokemon spawns, alerts are limited to pokemon, IV, CP, neighborhood, attack moves, and distance.\n"
    ">• For raids, alerts are limited to pokemon, attack moves, neighborhood, raid level, and distance.\n"
    ">• Adding an alert with only 1 different attribute from an existing alert will update your existing alert.\n"
    ">• Adding an alert with 2 or more different attributes from an existing alert will add a new alert.\n"
    "\nIf you need any additional guidance, please reach out to my creator, *zenyplz*!".format(MAX, RMAX)
)

RAID_HELP = (
    "*Getting Started*\n>Willow can now notify you of raids!\n"
    ">`raid` or `raid help` - View this help screen.\n"
    ">`raid list` - View all of your raid alerts.\n"
    ">`raid notify snorlax, muk, lapras` - Add 3 alerts: Snorlax raids, Muk raids, and Lapras raids.\n"
    ">`raid alert lapras blizzard 1000m` - Alert raids for Lapras with Blizzard within 1000m of your set location.\n"
    ">`raid notify level4` or `raid alert 4` - Notify all level4 raids.\n"
    ">`raid alert egg encino` - Add an alert for all eggs found in Encino.\n"
    ">`raid remove 1, 3-7, 15+` - Remove raid alert(s) based on the listed order number.\n"
    ">`raid remove all` - Clear all raid alerts.\n"
    "*Important Things To Note*\n"
    ">• Each trainer is only allowed a maximum of {} raid alerts.\n"
    ">• Raid alerts are limited to pokemon, attack moves, neighborhood, raid level, and distance.\n"
    ">• By default, alerts only search for hatched raids. If you want eggs, specify `egg` in your alert.\n"
    .format(RMAX)
)

########################################################################################################################

ADMIN_HELP = (
    "`admin list <name>` - Lists all triggers for <name>\n"
    "`admin location <name>`\n"
    "`admin notify/alert <name> <alerts>` - Adds a new alert for <name>\n"
    "`admin remove <name> <#/all>` - Removes alert #/all for <name>\n"
    "`admin raid notify/alert <name> <alerts>`\n"
    "`admin raid remove/delete <name> <#/all>`\n"
    "`admin mute/unmute <name>` - Mutes/Unmutes willow for <name>\n"
    "`admin print` - Prints out database items to output/"
)
