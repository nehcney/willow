# Standard Library Imports
import re
import logging

# Local libraries
import config
import msg
from utils import parseint, get_gmaps_link
from db import update_triggers, update_raid_triggers, update_directory, update_raid_directory, update_users
from user import NewDirectoryEntry, RaidDirectoryEntry


logger = logging.getLogger('willow')


########################################################################################################################
#                                                    COMMANDS                                                          #
########################################################################################################################

def parse_location(user_id, name, args):
    """
    :param user_id: user's slack id (str)
    :param name: user's slack nickname (str)
    :param args: stripped string of location arguments (str)
    :return: message (str)
    """
    # Location without arguments
    if not args:
        logger.debug("{} wants to view location info.".format(name))
        if config.USERS[user_id].coord:
            lat, lng = config.USERS[user_id].coord
            message = "Your location is set to: "
            if config.USERS[user_id].loc:
                message += " *{}*  ".format(config.USERS[user_id].loc)
            message += " <{}|{}, {}>".format(get_gmaps_link(lat, lng), lat, lng)
            if config.USERS[user_id].locations:
                message += "\n>:star: Your saved locations: :star:"
                for loc_name, coords in config.USERS[user_id].locations.items():
                    message += "\n>*{}*   <{}|{}, {}>".format(
                        loc_name, get_gmaps_link(coords[0], coords[1]), coords[0], coords[1])
            message += "\n" + msg.LOC_USAGE + "\n" + msg.LOC_SAVE + "\n" + msg.LOC_LOAD + "\n" + msg.WEB_TOOL
        else:
            message = ">You have not set a location."
            message += "\n" + msg.LOC_USAGE + "\n" + msg.LOC_SAVE + "\n" + msg.LOC_LOAD + "\n" + msg.WEB_TOOL
        return message

    # Location with arguments
    loc_name, _, coords = args.strip().partition(' ')
    loc_name = loc_name.lower()
    if loc_name.isalpha():

        # Location <delete/remove>
        if loc_name in ('delete', 'remove'):
            loc_name = coords.strip().lower()
            logger.debug("{} wants to delete a saved location: {}".format(name, loc_name))
            try:
                del config.USERS[user_id].locations[loc_name]
                if config.USERS[user_id].loc not in config.USERS[user_id].locations:
                    config.USERS[user_id].loc = ""
                update_users()
                logger.debug("Delete successful!")
                return "Successfully deleted your saved location: *{}*".format(loc_name)
            except KeyError:
                logger.warning("Saved location not found: {}".format(loc_name))
                return "You do not have a saved location named: *{}*".format(loc_name)

        # Location <alpha>
        if not coords:
            logger.debug("{} wants to load saved location '{}'.".format(name, loc_name))
            try:
                coords = config.USERS[user_id].locations[loc_name]
                config.USERS[user_id].coord = coords
                config.USERS[user_id].loc = loc_name
                update_users()
                logger.debug("{} loaded saved location '{}'.".format(name, loc_name))
                return "Your location has now been set to *{}*: <{}|{}, {}>".format(
                    loc_name, get_gmaps_link(coords[0], coords[1]), coords[0], coords[1])
            except KeyError:
                logger.debug("{} tried to load a non-existant location '{}'.".format(name, loc_name))
                message = "You do not have a *{}* location saved.".format(loc_name)
                message += "\n" + msg.LOC_SAVE + "\n" + msg.LOC_MORE_HELP
                return message

        # Location <alpha> <coords(?)>
        logger.debug("{} wants to save location '{}' to: {}".format(name, loc_name, coords))
        coords = list(filter(None, coords.split(',')))
        return __set_location(user_id, name, coords, loc_name)

    # Location <coords>
    logger.debug("{} wants to set location without saving.".format(name))
    coords = list(filter(None, args.split(',')))
    return __set_location(user_id, name, coords)


def add_notification(user_id, name, alerts, override=False):
    """
    :param user_id: user's slack id
    :param name: user's slack nickname
    :param alerts: an unprocessed list of alerts, eg. [' DrAGoNite 90+', 'snorlax  45cp- ']
    :param override: whether to override limits such as MAX_ALERTS or proper spelling
    :return: message string
    """
    # Handle missing alerts
    if not alerts:
        logger.debug("Nothing to be alerted of: missing parameters")
        return msg.MISSING + '\n' + msg.ALERT_USAGE

    # Handle exceeds max alerts
    num_current_alerts = len(config.DIRECTORY[user_id]) if user_id in config.DIRECTORY else 0
    if not override and len(alerts) + num_current_alerts > config.MAX_ALERTS:
        logger.debug("{} will exceed the limit by adding the ({}) alert(s)! Aborting.".format(name, len(alerts)))
        return msg.OVER_MAX

    logger.debug("Adding alert(s) for {}".format(name))

    # Parse the list of alerts into a list of unique, sorted lists
    _alerts = [sorted(set(alert.lower().split())) for alert in alerts]

    num_added = 0
    num_updated = 0
    num_exists = 0
    num_invalid = 0
    message = ""

    # Process each alert in the list
    # -~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~ #
    for i, alert in enumerate(_alerts):

        # Check for valid inputs and copy them to another list
        valid_alert, iv, cp, distance, message = __process_alert(user_id, alert, message, override)
        if not valid_alert:
            if iv or cp or distance:
                logger.warning("User attempted to add IV/CP/distance alone.")
                message += msg.INVALID + "*{}*".format(alerts[i]) + "\n" + msg.NUM_ONLY + "\n"
            num_invalid += 1
            continue

        # Attempt to add notification to triggers + directory
        found = False
        if config.DIRECTORY.get(user_id):
            for e_pos, entry in enumerate(config.DIRECTORY[user_id]):
                if entry[0] == valid_alert:
                    iv_match = True if entry[1].iv == iv else False
                    cp_match = True if entry[1].cp == cp else False
                    di_match = True if entry[1].distance == distance else False

                    # Exact entry already exists! Exit.
                    if iv_match and cp_match and di_match:
                        logger.debug("Alert matches pre-existing alert: nothing to do")
                        num_exists += 1
                        found = True
                        break

                    # All of IV+CP+distance are different
                    if not iv_match and not cp_match and not di_match:
                        continue

                    # Only one of IV/CP/distance is different
                    elif not iv_match ^ cp_match ^ di_match:
                        # Update trigger entries
                        alert_tuple = tuple(valid_alert)
                        old_tuple = (user_id, alert_tuple, entry[1].iv, entry[1].cp, entry[1].distance)
                        new_tuple = (user_id, alert_tuple, iv, cp, distance)
                        if not __update_trigger_entries(valid_alert, old_tuple, new_tuple):
                            return msg.ERROR

                        # Update directory entry
                        attributes = ""
                        if not iv_match:
                            config.DIRECTORY[user_id][e_pos][1].iv = iv
                            attributes += " + IV" if attributes else "IV"
                        if not cp_match:
                            config.DIRECTORY[user_id][e_pos][1].cp = cp
                            attributes += " + CP" if attributes else "CP"
                        if not di_match:
                            config.DIRECTORY[user_id][e_pos][1].distance = distance
                            attributes += " + distance" if attributes else "distance"
                        logger.debug("Directory entry ({}) updated with new {}.".format(valid_alert, attributes))
                        num_updated += 1
                        found = True
                        break

        if not found:  # new alert
            logger.debug("{} does not have this alert. Adding it as a new alert.".format(name))
            __add_to_directory(user_id, valid_alert, iv, cp, distance)
            __add_to_triggers(user_id, valid_alert, iv, cp, distance)
            num_added += 1
    # -~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~ #

    # Update database
    update_directory()
    update_triggers()

    # Return completed message
    if num_invalid:
        message += \
            ":wavy_dash::wavy_dash::wavy_dash::wavy_dash::wavy_dash::wavy_dash::wavy_dash::wavy_dash::wavy_dash:\n"
    if num_added:
        s = "s" if num_added > 1 else ""
        message += "(*{}*) notification{} added.\n".format(num_added, s)
    if num_updated:
        s = "s" if num_updated > 1 else ""
        message += "(*{}*) notification{} updated.\n".format(num_updated, s)
    if num_exists:
        s = "s" if num_exists > 1 else ""
        message += "(*{}*) notification{} unchanged.\n".format(num_exists, s)
    if num_invalid:
        s = "s" if num_invalid > 1 else ""
        message += "Found (*{}*) invalid notification{}!".format(num_invalid, s)
    return message


def add_raid_notification(user_id, name, alerts, override=False):
    """
    :param user_id: user's slack id
    :param name: user's slack nickname
    :param alerts: an unprocessed list of alerts, eg. [' SnOrLaX 900m', 'snorlax  ENCINO ']
    :param override: whether to override limits such as MAX_RAID_ALERTS or proper spelling
    :return: message string
    """
    # Handle missing alerts
    if not alerts:
        logger.debug("Nothing to be alerted of: missing parameters")
        return msg.MISSING + '\n' + msg.RAID_ALERT_USAGE

    # Handle exceeds max alerts
    num_current_alerts = len(config.RAID_DIRECTORY.get(user_id, []))
    if not override and len(alerts) + num_current_alerts > config.MAX_RAID_ALERTS:
        logger.debug("{} will exceed the limit by adding the ({}) alert(s)! Aborting.".format(name, len(alerts)))
        return msg.OVER_RMAX

    logger.debug("Adding raid alert(s) for {}".format(name))

    # Parse the list of alerts into a list of unique, sorted lists
    _alerts = [sorted(set(alert.lower().split())) for alert in alerts]

    num_added = 0
    num_updated = 0
    num_exists = 0
    num_invalid = 0
    message = ""

    # Process each alert in the list
    # -~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~ #
    for i, alert in enumerate(_alerts):

        # Check for valid inputs and copy them to another list
        valid_alert, lvl, distance, egg, message = __process_raid_alert(user_id, alert, message, override)
        if valid_alert is None:
            num_invalid += 1
            continue
        invalid_egg = False
        for word in valid_alert:
            if egg and word not in config.NEIGHBORHOODS_SPLIT:
                invalid_egg = True
                break
        if invalid_egg:
            logger.warning("User attempted to add a pokemon/move against an egg LOL!")
            message += msg.INVALID + "*{}*".format(alerts[i]) + "\n" + msg.INVALID_EGG + "\n"
            num_invalid += 1
            continue

        # Attempt to add notification to triggers + directory
        if not valid_alert:
            valid_alert.append('all')
        found = False
        if config.RAID_DIRECTORY.get(user_id):
            for e_pos, entry in enumerate(config.RAID_DIRECTORY[user_id]):
                if entry[0] == valid_alert:
                    lvl_match = True if entry[1].lvl == lvl else False
                    dis_match = True if entry[1].distance == distance else False
                    egg_match = True if entry[1].egg == egg else False

                    # Exact entry already exists! Exit.
                    if lvl_match and dis_match and egg_match:
                        logger.debug("Alert matches pre-existing raid alert: nothing to do")
                        num_exists += 1
                        found = True
                        break

                    # All of level + distance + egg are different
                    if not lvl_match and not dis_match and not egg_match:
                        continue

                    # Only one of level/distance/egg is different
                    elif not lvl_match ^ dis_match ^ egg_match:
                        # Update trigger entries
                        alert_tuple = tuple(valid_alert)
                        old_tuple = (user_id, alert_tuple, entry[1].lvl, entry[1].distance, entry[1].egg)
                        new_tuple = (user_id, alert_tuple, lvl, distance, egg)
                        if not __update_raid_trigger_entries(valid_alert, old_tuple, new_tuple):
                            return msg.ERROR

                        # Update directory entry
                        attributes = ""
                        if not lvl_match:
                            config.RAID_DIRECTORY[user_id][e_pos][1].lvl = lvl
                            attributes += " + level" if attributes else "level"
                        if not dis_match:
                            config.RAID_DIRECTORY[user_id][e_pos][1].distance = distance
                            attributes += " + distance" if attributes else "distance"
                        if not egg_match:
                            config.RAID_DIRECTORY[user_id][e_pos][1].egg = egg
                            attributes += " + egg" if attributes else "egg"
                        logger.debug("Directory entry ({}) updated with new {}.".format(valid_alert, attributes))
                        num_updated += 1
                        found = True
                        break

        if not found:  # new alert
            logger.debug("{} does not have this raid alert. Adding it as a new raid alert.".format(name))
            __add_to_raid_directory(user_id, valid_alert, lvl, distance, egg)
            __add_to_raid_triggers(user_id, valid_alert, lvl, distance, egg)
            num_added += 1
    # -~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~-~=~ #

    # Update database
    update_raid_directory()
    update_raid_triggers()

    # Return completed message
    if num_invalid:
        message += \
            ":wavy_dash::wavy_dash::wavy_dash::wavy_dash::wavy_dash::wavy_dash::wavy_dash::wavy_dash::wavy_dash:\n"
    if num_added:
        s = "s" if num_added > 1 else ""
        message += "(*{}*) raid notification{} added.\n".format(num_added, s)
    if num_updated:
        s = "s" if num_updated > 1 else ""
        message += "(*{}*) raid notification{} updated.\n".format(num_updated, s)
    if num_exists:
        s = "s" if num_exists > 1 else ""
        message += "(*{}*) raid notification{} unchanged.\n".format(num_exists, s)
    if num_invalid:
        s = "s" if num_invalid > 1 else ""
        message += "Found (*{}*) invalid raid notification{}!".format(num_invalid, s)
    return message


def remove(user_id, name, args, raid=False):
    """
    :param user_id: user's slack id
    :param name: user's slack nickname
    :param args: unordered list of items
    :param raid: remove raid vs. pokemon alerts
    :return: message string
    """
    if not args:
        return msg.REMOVE_RAID_USAGE if raid else msg.REMOVE_USAGE
    if raid and not config.RAID_DIRECTORY.get(user_id):
        logger.warning("{} does not have any raid notifications".format(name))
        return msg.NONE_RAID
    elif not raid and not config.DIRECTORY.get(user_id):
        logger.warning("{} does not have any notifications".format(name))
        return msg.NONE

    # Parse args
    int_items, message = __process_remove_args(user_id, name, args, raid)
    if message:
        return message

    # Remove all items from list
    for num in int_items:
        index = num - 1
        target_tuple = (
            user_id,
            tuple(config.RAID_DIRECTORY[user_id][index][0]),
            config.RAID_DIRECTORY[user_id][index][1].lvl,
            config.RAID_DIRECTORY[user_id][index][1].distance,
            config.RAID_DIRECTORY[user_id][index][1].egg,
        ) if raid else (
            user_id,
            tuple(config.DIRECTORY[user_id][index][0]),
            config.DIRECTORY[user_id][index][1].iv,
            config.DIRECTORY[user_id][index][1].cp,
            config.DIRECTORY[user_id][index][1].distance
        )
        alert = config.RAID_DIRECTORY[user_id][index][0] if raid else config.DIRECTORY[user_id][index][0]

        # Delete trigger entries
        for word in alert:
            try:
                if raid:
                    del config.RAID_TRIGGERS[word][target_tuple]
                    if not config.RAID_TRIGGERS[word]:
                        del config.RAID_TRIGGERS[word]
                else:
                    del config.TRIGGERS[word][target_tuple]
                    if not config.TRIGGERS[word]:  # trigger has no registered users
                        del config.TRIGGERS[word]  # remove the entire trigger
            except KeyError as e:
                logger.error("Either trigger ({}) or tuple not found! -> {}".format(word, e))
                return msg.ERROR

        # Delete directory entry
        if raid:
            del config.RAID_DIRECTORY[user_id][index]  # leaves an empty list if all entries removed
        else:
            del config.DIRECTORY[user_id][index]  # leaves an empty list if all entries removed

    # Update database
    if raid:
        update_raid_directory()
        update_raid_triggers()
    else:
        update_directory()
        update_triggers()

    # Return appropriate message
    if args[0] == 'all':
        return msg.CLEARED_RAID if raid else msg.CLEARED
    item_list = ''
    for i in reversed(int_items):
        item_list += str(i) if not item_list else ', {}'.format(i)
    return msg.REMOVED_RAID + item_list if raid else msg.REMOVED + item_list


def list_directory(user_id, name, raid=False):
    logger.debug("Getting {}'s {}list.".format(name, "raid " if raid else ""))
    if raid and not config.RAID_DIRECTORY.get(user_id):
        logger.debug("User has no raid notifications")
        return msg.NONE_RAID
    elif not raid and not config.DIRECTORY.get(user_id):
        logger.debug("User has no notifications")
        return msg.NONE
    status = "*OFF* :x:" if config.USERS[user_id].mute else "*ON* :white_check_mark:"
    text_block = "Your {}notifications are: {}\n".format("raid " if raid else "", status)
    if config.USERS[user_id].mute:
        text_block += "To turn them back on, type: `unmute`\n"
    if raid:
        for i, entry in enumerate(config.RAID_DIRECTORY[user_id]):
            alert_string = "All raids..." if entry[0] == ['all'] else ', '.join(entry[0])
            lvl_string = " | *Level:* {}{}".format(entry[1].lvl[0], entry[1].lvl[1]) if entry[1].lvl[0] >= 0 else ""
            dis_string = " | *Distance:* {}m".format(entry[1].distance) if entry[1].distance >= 0 else ""
            egg_string = " | *Egg:* {}".format("Yes") if entry[1].egg else ""
            text_block += ">{}.  {}{}{}{}\n".format(i + 1, alert_string, lvl_string, dis_string, egg_string)
    else:
        for i, entry in enumerate(config.DIRECTORY[user_id]):
            iv_string = " | *IV:* {}%{}".format(entry[1].iv[0], entry[1].iv[1]) if entry[1].iv[0] >= 0 else ""
            cp_string = " | *CP:* {}{}".format(entry[1].cp[0], entry[1].cp[1]) if entry[1].cp[0] >= 0 else ""
            di_string = " | *Distance:* {}m".format(entry[1].distance) if entry[1].distance >= 0 else ""
            text_block += ">{}.  {}{}{}{}\n".format(i + 1, ', '.join(entry[0]), iv_string, cp_string, di_string)
    text_block += msg.REMOVE_RAID_USAGE if raid else msg.REMOVE_USAGE
    return text_block


def mute(user_id, name, flag=True):
    try:
        config.USERS[user_id].mute = flag
        update_users()
        logger.debug("{}'s notifications have been {}.".format(name, "disabled" if flag else "enabled"))
        return msg.MUTE if flag else msg.UNMUTE
    except KeyError:
        logger.error("User not found in database!")
        return msg.ERROR


########################################################################################################################
#                                                    HELPERS                                                           #
########################################################################################################################

def __set_location(user_id, name, coords, loc_name=""):
    """
    :param user_id: user's slack id
    :param name: user's slack nickname
    :param coords: list of coordinates, eg. ['34.4', '-118.5']
    :param loc_name: optional location name (lowercase) to save coords to
    :return: message (str)
    """
    if not len(coords) == 2:
        logger.warning("User did not define two arguments for coordinates.")
        message = ">Invalid arguments: *{}*\n".format(", ".join(coords))
        message += (
            "Reminder: saved location names cannot contain numbers, symbols, or spaces,\n"
            "and coordinates must be of the format *latitude, longitude* with no parenthesis.\n")
        return message + msg.LOC_MORE_HELP

    try:
        lat = float(coords[0])
        lng = float(coords[1])
    except ValueError as e:
        logger.warning("Coordinates are of an invalid format -> {}".format(e))
        return ">Invalid coordinates: {}".format(', '.join(coords)) + "\n" + msg.LOC_USAGE

    if not (-90 <= round(lat) <= 90) or not (-180 <= round(lng) <= 180):
        logger.warning("Coordinates out of bounds: ({}, {})".format(lat, lng))
        return ">Invalid coordinates: {}, {}".format(lat, lng) + "\n" + msg.LOC_USAGE

    if loc_name:
        config.USERS[user_id].locations[loc_name] = (lat, lng)
        message = "The coordinates <{}|{}, {}> have been saved to: *{}*\n".format(
            get_gmaps_link(lat, lng), lat, lng, loc_name)
        message += "To load this saved location, type `location {}`".format(loc_name)
        return message

    config.USERS[user_id].coord = (lat, lng)
    update_users()
    logger.debug("{} has saved location '{}' to: ({}, {})".format(name, loc_name, lat, lng))
    return "Your current location has been set: <{}|{}, {}>".format(get_gmaps_link(lat, lng), lat, lng)


def __process_alert(user_id, alert, message, override=False):
    """
    Processes a list of alert words and returns the appropriate values.

    :param user_id: user's slack id
    :param alert: list of words (str)
    :param override: whether to override limits such as proper spelling
    :return: list of valid trigger words (str), iv, cp, distance, and message (str)
    """
    if not alert:
        logger.warning("Empty alert list found.")
        message += msg.INVALID + "\n" + msg.MISSING_WORDS + "\n"
        return None, None, None, None, message

    iv = (-1, '+')
    cp = (-1, '+')
    distance = -1
    valid_alert = []
    for word in alert:
        if override or word in config.DICTIONARY:
            valid_alert.append(word)
        else:
            # Could be a misspelled word, or an IV/CP/distance rule. Let's check!
            digits = parseint(word)
            if digits is not None:
                alpha = re.sub(r'[\d%.=+-]', '', word)
                if not alpha or alpha == 'iv':  # treat this as an IV rule
                    if digits > 100:
                        logger.warning("Specified IV is over 100! ({})".format(digits))
                        message += msg.INVALID + "*{}*".format(word) + "\n" + msg.IV_RANGE + "\n"
                        return None, None, None, None, message
                    sign = re.sub(r'[^=+-]', '', word)
                    iv = (digits, sign[-1] if sign else '+')
                    continue
                elif alpha == 'cp':  # this is a CP rule
                    sign = re.sub(r'[^=+-]', '', word)
                    cp = (digits, sign[-1] if sign else '+')
                    continue
                elif alpha in ('m', 'meters'):
                    if not config.USERS[user_id].coord:
                        logger.warning("User wants to add distance rule without first setting home location.")
                        message += "{}*{}*\n{}\n{}\n".format(msg.INVALID, word, msg.NO_LOC, msg.LOC_USAGE)
                        return None, None, None, None, message
                    distance = digits
                    continue
            # Misspelled word
            logger.warning("Word spelled incorrectly: {}".format(word))
            message += msg.INVALID + "*{}*".format(word) + "\n" + msg.MISSPELLED_WORD + "\n"
            return None, None, None, None, message

    return valid_alert, iv, cp, distance, message


def __process_raid_alert(user_id, alert, message, override=False):
    """
    Processes a list of raid alert words and returns the appropriate values.

    :param user_id: user's slack id
    :param alert: list of words (str)
    :param override: whether to override limits such as proper spelling
    :return: list of valid trigger words (str), level, distance, egg, and message (str)
    """
    if not alert:
        logger.warning("Empty raid alert list found.")
        message += msg.INVALID + "\n" + msg.MISSING_WORDS + "\n"
        return None, None, None, None, message

    level = (-1, '+')
    distance = -1
    egg = False
    valid_alert = []
    for word in alert:
        if word in ('egg', 'eggs'):
            egg = True
        elif word in config.DICTIONARY:
            valid_alert.append(word)
        else:
            # Could be a misspelled word, or a level/distance rule. Let's check!
            digits = parseint(word)
            if digits is not None:
                alpha = re.sub(r'[\d%.=+-]', '', word)
                if not alpha or alpha == 'level':  # treat this as an level rule
                    if digits < 1 or digits > 5:
                        logger.warning("Specified level not in range! ({})".format(digits))
                        message += msg.INVALID + "*{}*".format(word) + "\n" + msg.LVL_RANGE + "\n"
                        return None, None, None, None, message
                    sign = re.sub(r'[^=+-]', '', word)
                    level = (digits, sign[-1] if sign else '+')
                elif alpha in ('m', 'meters'):
                    if not config.USERS[user_id].coord:
                        logger.warning("User wants to add distance rule without first setting home location.")
                        message += "{}*{}*\n{}\n{}\n".format(msg.INVALID, word, msg.NO_LOC, msg.LOC_USAGE)
                        return None, None, None, None, message
                    distance = digits
            elif override:
                valid_alert.append(word)
            else:
                # Misspelled word
                logger.warning("Word spelled incorrectly: {}".format(word))
                message += msg.INVALID + "*{}*".format(word) + "\n" + msg.MISSPELLED_WORD + "\n"
                return None, None, None, None, message

    return valid_alert, level, distance, egg, message


def __add_to_directory(user_id, alerts, iv, cp, distance):
    try:
        config.DIRECTORY[user_id].append([alerts, NewDirectoryEntry(iv, cp, distance)])
        logger.debug("New directory entry added.")
    except KeyError:
        config.DIRECTORY[user_id] = [[alerts, NewDirectoryEntry(iv, cp, distance)]]
        logger.debug("First directory entry added.")


def __add_to_raid_directory(user_id, alerts, lvl, distance, egg):
    try:
        config.RAID_DIRECTORY[user_id].append([alerts, RaidDirectoryEntry(lvl, distance, egg)])
        logger.debug("New raid directory entry added.")
    except KeyError:
        config.RAID_DIRECTORY[user_id] = [[alerts, RaidDirectoryEntry(lvl, distance, egg)]]
        logger.debug("First raid directory entry added.")


def __add_to_triggers(user_id, alerts, iv, cp, distance):
    alert_tuple = tuple(alerts)
    new_tuple = (user_id, alert_tuple, iv, cp, distance)
    num_words = len(alerts)
    for word in alerts:
        try:
            config.TRIGGERS[word][new_tuple] = num_words
            logger.debug("Added user to existing trigger entry: ({})".format(word))
        except KeyError:
            config.TRIGGERS[word] = {new_tuple: num_words}
            logger.debug("Added new trigger entry: ({})".format(word))


def __add_to_raid_triggers(user_id, alerts, lvl, distance, egg):
    alert_tuple = tuple(alerts)
    new_tuple = (user_id, alert_tuple, lvl, distance, egg)
    num_words = len(alerts)
    for word in alerts:
        try:
            config.RAID_TRIGGERS[word][new_tuple] = num_words
            logger.debug("Added user to existing trigger entry: ({})".format(word))
        except KeyError:
            config.RAID_TRIGGERS[word] = {new_tuple: num_words}
            logger.debug("Added new trigger entry: ({})".format(word))


def __update_trigger_entries(alerts, old_tuple, new_tuple):
    num_words = len(alerts)
    for word in alerts:
        try:
            config.TRIGGERS[word][new_tuple] = config.TRIGGERS[word].pop(old_tuple)
            logger.debug("Updated trigger entry: ({})".format(word))
            num_words -= 1
        except KeyError as e:
            logger.error("Either trigger ({}) or tuple not found! -> {}".format(word, e))
    if num_words:
        logger.error("DID NOT UPDATE ALL ENTRIES!")
        return False
    else:
        logger.debug("Updated all trigger entries.")
        return True


def __update_raid_trigger_entries(alerts, old_tuple, new_tuple):
    num_words = len(alerts)
    for word in alerts:
        try:
            config.RAID_TRIGGERS[word][new_tuple] = config.RAID_TRIGGERS[word].pop(old_tuple)
            logger.debug("Updated trigger entry: ({})".format(word))
            num_words -= 1
        except KeyError as e:
            logger.error("Either trigger ({}) or tuple not found! -> {}".format(word, e))
    if num_words:
        logger.error("DID NOT UPDATE ALL ENTRIES!")
        return False
    else:
        logger.debug("Updated all trigger entries.")
        return True


def __process_remove_args(user_id, name, args, raid=False):
    pattern_range = re.compile('^(\d+)-(\d+)$')
    pattern_less_than = re.compile('^(\d+)-$')
    pattern_greater_than = re.compile('^(\d+)\+$')
    total_user_alerts = len(config.RAID_DIRECTORY[user_id]) if raid else len(config.DIRECTORY[user_id])
    logger.debug("{} currently has ({}) notifications.".format(name, total_user_alerts))
    if args[0].lower() == 'all':
        int_items = [num for num in range(total_user_alerts, 0, -1)]
    else:
        int_items = []
        for arg in reversed(args):
            if arg.isdigit():
                int_num = int(arg)
                if int_num < 1 or int_num > total_user_alerts:
                    logger.warning("Alert does not exist (out of bounds): {}".format(arg))
                    return (None, msg.OOB_RAID) if raid else (None, msg.OOB)
                int_items.append(int_num)
            elif pattern_range.match(arg):
                obj = pattern_range.match(arg)
                low = min(int(obj.group(1)), int(obj.group(2)))
                high = max(int(obj.group(1)), int(obj.group(2)))
                if low < 1 or high > total_user_alerts:
                    logger.warning("Alert does not exist (out of bounds): {}".format(arg))
                    return (None, msg.OOB_RAID) if raid else (None, msg.OOB)
                for i in range(high, low - 1, -1):
                    int_items.append(i)
            elif pattern_less_than.match(arg):
                num = int(pattern_less_than.match(arg).group(1))
                if num < 1 or num > total_user_alerts:
                    logger.warning("Alert does not exist (out of bounds): {}".format(arg))
                    return (None, msg.OOB_RAID) if raid else (None, msg.OOB)
                for i in range(num, 0, -1):
                    int_items.append(i)
            elif pattern_greater_than.match(arg):
                num = int(pattern_greater_than.match(arg).group(1))
                if num < 1 or num > total_user_alerts:
                    logger.warning("Alert does not exist (out of bounds): {}".format(arg))
                    return (None, msg.OOB_RAID) if raid else (None, msg.OOB)
                for i in range(total_user_alerts, num - 1, -1):
                    int_items.append(i)
            else:
                logger.warning("{} tried to remove some funky thang: ({})".format(name, arg))
                return (None, msg.REMOVE_RAID_USAGE) if raid else (None, msg.REMOVE_USAGE)
        int_items = sorted(set(int_items), reverse=True)

    return int_items, None
