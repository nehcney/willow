# -*- coding: utf-8 -*-

# Standard imports
import os
import logging
import logging.handlers


LOG_DIR = 'logs'
LOG_PATH = os.path.join(LOG_DIR, 'willow-debug.log')


def setup_logging():
    """
    Setup the willow logger.

    :return: logger object
    """

    logger = logging.getLogger('willow')
    logger.setLevel(logging.DEBUG)

    # Remove all attached handlers, in case there was
    # a logger using the name 'devstack'
    del logger.handlers[:]

    # Log format
    default_format = logging.Formatter('[%(asctime)s] [%(levelname)s] - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')

    # File handler for willow-debug.log
    if not os.path.exists(LOG_DIR):
        try:
            os.mkdir(LOG_DIR)
        except OSError as e:
            logger.error('Exception when creating {}: {}'.format(LOG_DIR, e))
    file_handler = logging.handlers.TimedRotatingFileHandler(LOG_PATH, when='midnight', interval=1)
    file_handler.setLevel(logging.DEBUG)
    file_handler.setFormatter(default_format)

    # Console Handler for stdout
    console_handler = logging.StreamHandler()
    console_handler.setLevel(logging.INFO)
    console_handler.setFormatter(default_format)

    # Adding Handlers
    logger.addHandler(console_handler)
    logger.addHandler(file_handler)

    return logger
